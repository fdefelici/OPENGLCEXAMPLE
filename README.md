# Obiettivo Progetto
Renderizzare 2 modelli da file obj (susanne e stormtrup) 
> Per stormtrup viene caricata anche la texture
> Per susanne viene scelto color casuale
> Applicato effetto "fatness"
> Applicata rotation su asse y
> Applicato effetto luce (lambert) sullo stormtrup

# Librerie usate
* 'xeasy' : per caricare file obj
* 'glad'  : per automatizzare caricamento lib opengl multipiattaform (opengl loader generator)
* 'stb'   : lib multifunzione (usata per caricare texture) [https://github.com/nothings/stb]


# References
## OpenGL Hadware Spec Viewer
http://realtech-vr.com/admin/glview  

## OpenGL
https://en.wikibooks.org/wiki/OpenGL_Programming
http://duriansoftware.com/joe/An-intro-to-modern-OpenGL.-Chapter-1:-The-Graphics-Pipeline.html

## Fong tutorial
https://www.tomdalling.com/blog/modern-opengl/07-more-lighting-ambient-specular-attenuation-gamma/
## Specular tutorial
http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=25