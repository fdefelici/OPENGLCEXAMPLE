#version 330 core

const vec3 worldLight = vec3(2,7,0);
const vec3 worldCamera = vec3(0,0,3);

layout(location=0) in vec3 vertex;
layout(location=1) in vec2 uv;
layout(location=2) in vec3 normal;

uniform float rotator;
uniform float fatness;
uniform vec3  localToWorldTransf;
uniform float scale;
uniform float rotationRayFix;
uniform float colorType;

out vec3 out_color;
out vec2 out_uv;
out vec3 out_lightDirection;
out vec3 out_vertexNormal;
out vec3 out_vertexTolightReflect;
out vec3 out_vertexToCameraNormal;

vec3 compute_perspective(vec3 v) {
    float fov = 60;
    float fovTan = tan(radians(60/2));
    float zNear = 1;
    float zFar =  1000;
    float zNearNew = -1;
    float zFarNew = 1;

    //-1 per invertire la Z rispetto allo standard opengl
    float zReversed = v.z * -1.0f;
    v.x = v.x / (zReversed * fovTan);
    v.y = v.y / (zReversed * fovTan);
    //Trasformiamo la Z del mondo di gioco nel dominio [-1, +1]
    // new_value = ( (old_value - old_min) / (old_max - old_min)) * (new_max - new_min) + new_min)
    v.z = ( (zReversed - zNear) / (zFar - zNear) * (zFarNew - zNearNew) + zNearNew);
    return v;
}

vec3 applyFatness(vec3 v, vec3 n, float fat) {
    return v + ( normalize(n) * fat );
}

vec3 rotate_y(vec3 v, float angle) {
    float angleRad = radians(angle);
    float x = v.x * cos(angleRad) + v.z * sin(angleRad);
    float z = v.x * -sin(angleRad) + v.z * cos(angleRad);
    float y = v.y;
    return vec3(x, y, z);
}
vec3 rotate_pivot_y(vec3 pivot, vec3 v, float angle) {
    if (rotationRayFix == 0) return v;
    float angleRad = radians(angle);
    float x = pivot.x + ( (v.x - pivot.x)/rotationRayFix*cos(angleRad) - (v.z - pivot.z)/rotationRayFix*sin(angleRad));
    float z = pivot.z - pivot.z + ( (v.x - pivot.x)/rotationRayFix*sin(angleRad) + (v.z - pivot.z)/rotationRayFix*cos(angleRad));
    float y = v.y/rotationRayFix;
    return vec3(x, y, z);
}

void main() {
    //vec3 localPos = rotate_y(vertex, rotator);
    vec3 worldPosition = vertex;
    worldPosition *= scale;
    worldPosition = rotate_pivot_y(vec3(0,0,-5), worldPosition, rotator);
    //worldPosition = rotate_y(worldPosition, rotator);
    worldPosition = worldPosition + localToWorldTransf;                     // Vertex position in "World" coordinates
    
    vec3 worldNormal = normalize( rotate_pivot_y(vec3(0,0,-5), normal, rotator));               
    //vec3 worldNormal = normalize( rotate_y(normal, rotator));                                
    vec3 perspPos = compute_perspective(applyFatness(worldPosition, worldNormal, fatness));  
    vec3 worldLightToVertexDirection = normalize(worldLight - worldPosition);                
    vec3 worldVertexToCameraNormal = normalize(worldCamera - worldPosition);                 
    vec3 worldVertexToLightReflect = reflect(-worldLightToVertexDirection,  worldNormal);  

    gl_Position = vec4(perspPos, 1);
    out_uv = uv;
    out_vertexNormal = worldNormal; 
    out_lightDirection = worldLightToVertexDirection;
    out_vertexToCameraNormal = worldVertexToCameraNormal; 
    out_vertexTolightReflect = worldVertexToLightReflect;
    
    if (colorType == -1)  out_color = normalize(vertex);  //Colore causale normalizzando x, y, z tra [0, 1]
    else if (colorType == 1) out_color = vec3(0.6, 0.6, 0.6);
    else if (colorType == 2) out_color = vec3(1, 1, 0); 
}