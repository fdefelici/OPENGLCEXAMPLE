#version 330 core

const float ambientFactor = 0.5;
const float lightIntesity = 0.3;
const float materialShiness = 0.4;
const vec3  specularBaseColor = vec3(1,1,1);

uniform sampler2D tex;
uniform bool useTexture;

in vec2 out_uv;
in vec3 out_lightDirection;
in vec3 out_vertexNormal;
in vec3 out_color;
in vec3 out_vertexTolightReflect;
in vec3 out_vertexToCameraNormal;

out vec4 frag_color;

void main() {   
    //NOTA: Sostituire IF con funzione di blending?!?
    vec3 baseColor;
    if (useTexture) {
        baseColor = texture2D(tex, out_uv).xyz;
    } else {
        baseColor = out_color;
    }

    //Ambient 
    vec3 ambientColor = baseColor * ambientFactor;

    //Diffuse
    float lambert = clamp(dot(out_lightDirection, out_vertexNormal), 0, 1);
    vec3 diffuseColor = baseColor * lambert * lightIntesity;
        
    //Specular 
    float cosAngle = clamp(dot(out_vertexTolightReflect, out_vertexToCameraNormal), 0, 1);
    float specularFactor = pow(cosAngle, materialShiness);
    vec3 specularColor = specularFactor * specularBaseColor * lightIntesity;

    //FONG
    vec3 finalColor = ambientColor + diffuseColor + specularColor;
    //vec3 finalColor = baseColor;
    frag_color = vec4(finalColor, 1);
}