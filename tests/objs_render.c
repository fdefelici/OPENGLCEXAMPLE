#include <aiv_opengl.h>
#include <stdio.h>
#include <stdlib.h>
#include <xeasy.h>
//Per specificare a stb_image di includere il codice C presente nel .h
#define STB_IMAGE_IMPLEMENTATION   
#include <stb_image.h>

GLuint aiv_compile_shader(char *, char *);

aiv_opengl_context_t context;

int bufferizeObj(const char* objFilePath, GLuint *vao_out, int *vertexSize_out) {
    XWObj *obj = XWObj_fromFile(objFilePath, XWOBJ_LOADSTRATEGY_OPENGL);
    
    int triSize = XList_size(obj->triangles);
    int verSize = triSize * 3;
    int pointSize = verSize * 3;

    GLfloat *vertices = malloc(sizeof(GLfloat) * pointSize);
    ///NOTA CONVERSIONE OBJ IN COORDINATE MONDO (obj ha coordinate locali che dipendono da come è stato fatto l'asset dal grafico.)
    // Conviene applicare il riadattamenteo nello shader cosi se la mesh si muove si ricalcola anche le coordinate mondo
    int vertId=0;
    for(int i=0; i<triSize; i++) {
        XTriangle *each = XList_at(obj->triangles, i);
        vertices[vertId] = each->v1.position.x; vertId++;
        vertices[vertId] = each->v1.position.y; vertId++;
        vertices[vertId] = each->v1.position.z; vertId++;

        vertices[vertId] = each->v2.position.x; vertId++;
        vertices[vertId] = each->v2.position.y; vertId++;
        vertices[vertId] = each->v2.position.z; vertId++;

        vertices[vertId] = each->v3.position.x; vertId++;
        vertices[vertId] = each->v3.position.y; vertId++;
        vertices[vertId] = each->v3.position.z; vertId++;
    }
    
    //CARICAMENTO UV
    int uvSize = triSize * 3;
    int uvPointSize = uvSize * 2;

    GLfloat *uvs = malloc(sizeof(GLfloat) * uvPointSize);
    vertId=0;
    for(int i=0; i<triSize; i++) {
        XTriangle *each = XList_at(obj->triangles, i);
        uvs[vertId] = each->v1.uv.x; vertId++;
        uvs[vertId] = each->v1.uv.y; vertId++;

        uvs[vertId] = each->v2.uv.x; vertId++;
        uvs[vertId] = each->v2.uv.y; vertId++;

        uvs[vertId] = each->v3.uv.x; vertId++;
        uvs[vertId] = each->v3.uv.y; vertId++;
    }

    //CARICAMENTO NORMALI
    int normSize = triSize * 3;
    int normPointSize = normSize * 3;

    GLfloat *normals = malloc(sizeof(GLfloat) * normPointSize);
    vertId=0;
    for(int i=0; i<triSize; i++) {
        XTriangle *each = XList_at(obj->triangles, i);
        normals[vertId] = each->v1.normal.x; vertId++;
        normals[vertId] = each->v1.normal.y; vertId++;
        normals[vertId] = each->v1.normal.z; vertId++;

        normals[vertId] = each->v2.normal.x; vertId++;
        normals[vertId] = each->v2.normal.y; vertId++;
        normals[vertId] = each->v2.normal.z; vertId++;

        normals[vertId] = each->v3.normal.x; vertId++;
        normals[vertId] = each->v3.normal.y; vertId++;
        normals[vertId] = each->v3.normal.z; vertId++;
    }

    GLuint vao;
    glGenVertexArrays(1, &vao);
    if (vao == 0) {
        fprintf(stderr,"unable to create VAO\n");
        aiv_opengl_context_destroy(&context);
        return -1;
    }
    fprintf(stdout, "VAO created with id %d\n", vao);
    glBindVertexArray(vao);

    GLuint vbo[3];
    glGenBuffers(3, vbo);  //Crea 3 buffer (VBO) legandoli al VAO correntemente bindato con "glBindVertexArray"
    
    //buffer 0: Vertex
    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);    //lega il buffer (VBO) al gruppo "Array Buffer"
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * pointSize, vertices, GL_STATIC_DRAW); //Esegue upload dei dati nel buffer VBO
    GLuint channel0 = 0;                     //I canali sono numerati a partire da 0.
    glEnableVertexAttribArray(channel0);     //Chiedo di aprire il canale
    glVertexAttribPointer(channel0, 3, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*3, (void *)0); 

    //buffer 1: UV
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);   
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * uvPointSize, uvs, GL_STATIC_DRAW);
    GLuint channel1 = 1;
    glEnableVertexAttribArray(channel1);  
    glVertexAttribPointer(channel1, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*2, (void *)0); 

    //buffer 2: NORMALS
    glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);   
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * normPointSize, normals, GL_STATIC_DRAW);
    GLuint channel2 = 2;
    glEnableVertexAttribArray(channel2);     
    glVertexAttribPointer(channel2, 3, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*3, (void *)0);     

    *vao_out = vao;
    *vertexSize_out = verSize;
    return 1;
}

int main(int argc, char *argv[])
{
    context.width = 800;
    context.height = 800;
    context.title = "StoormTrup";
    context.major = 3;
    context.minor = 3;
    
    GLuint ctxFails = aiv_opengl_context_init(&context);
    if (ctxFails) { fprintf(stderr, "error while initializing aiv_opengl_context\n"); return -1; }
    
    GLuint program = aiv_compile_shader("shaders/vertex.glsl", "shaders/fragment.glsl");
    if (!program) { fprintf(stderr,"unable to build shader program\n"); aiv_opengl_context_destroy(&context); return -1; }

    glViewport(0, 0, context.width, context.height);   //Specifies the affine transformation of x and y from normalized device coordinates to window coordinate
    glEnable(GL_DEPTH_TEST);                           //Abilito Z-BUFFER
    glClearColor(0, 0, 0, 1);                          //Imposta BLACK come clear color
    glUseProgram(program);                             //Specifica programma da usare 
    
    //Create VAO for Trup
    GLuint vao_trup;
    int vertSize_trup;
    bufferizeObj("res/trup.obj", &vao_trup, &vertSize_trup);

    //Create VAO for Susannne
    GLuint vao_susanne;
    int vertSize_susanne;
    bufferizeObj("res/susanne.obj", &vao_susanne, &vertSize_susanne);

    //Create VAO for Plane
    GLuint vao_plane;
    int vertSize_plane;
    bufferizeObj("res/plane.obj", &vao_plane, &vertSize_plane);
    //printf("plane: %d\n", vertSize_plane);

    //Caricamento Texture
    int width, height, components;
    unsigned char* image = stbi_load("res/stormtrooper.png", &width, &height, &components, 0);     
    GLuint textureId;
    glGenTextures(1, &textureId);
    glActiveTexture(GL_TEXTURE0);             // Attiva texture unit
    glBindTexture(GL_TEXTURE_2D, textureId);  // Binding rispetto alla unit corrente
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    // Impostare il Filtering obbligatorio altrimenti la scheda non renderizza nulla
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);  //Applica Filtro lineare quando la risoluzione texture e' minore dello schermo
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);  //Applica Filtro lineare quando la risoluzione texture e' maggiore dello schermo
    glUniform1i(glGetUniformLocation(program, "tex"), 0);

    float fatness_trup = 0;
    float fatness_susanne = 0;
    float rotator_trup = 0;
    float rotator_susanne = 0;
    
    //float rotator_pivot_susanne = 0;
    
    while(!context.closed) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  //Oltre alla pulizia dei buffer, pulisco anche lo Z-Buffer abilitato precedentemente
    
        glBindVertexArray(vao_trup);
            glUniform3f(glGetUniformLocation(program, "localToWorldTransf"), 0, -2, -5);
            glUniform1f(glGetUniformLocation(program, "rotator"), 0); 
            glUniform1f(glGetUniformLocation(program, "rotationRayFix"), 0);
            glUniform1f(glGetUniformLocation(program, "fatness"), 0); 
            glUniform1f(glGetUniformLocation(program, "scale"), 1);
            glUniform1i(glGetUniformLocation(program, "useTexture"), GL_TRUE); 
        glDrawArrays(GL_TRIANGLES, 0, vertSize_trup);  // DRAW CALL
    
        rotator_susanne+=0.05f;
        glBindVertexArray(vao_susanne);
            glUniform3f(glGetUniformLocation(program, "localToWorldTransf"), 0, 0.5, -5);
            glUniform1f(glGetUniformLocation(program, "rotator"), -rotator_susanne);
            glUniform1f(glGetUniformLocation(program, "rotationRayFix"), 4);
            glUniform1f(glGetUniformLocation(program, "fatness"), 0);  
            //glUniform1f(glGetUniformLocation(program, "fatness"), 0);
            glUniform1f(glGetUniformLocation(program, "scale"), 1);
            glUniform1i(glGetUniformLocation(program, "useTexture"), GL_FALSE);
            glUniform1f(glGetUniformLocation(program, "colorType"), -1); //RANDOM
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL

        glUniform1f(glGetUniformLocation(program, "fatness"), fabsf((float)sin(fatness_susanne+=0.0005))/5.0f);  
        glUniform3f(glGetUniformLocation(program, "localToWorldTransf"), 0, 1.5, -5);
        glUniform1f(glGetUniformLocation(program, "rotator"), 10 + rotator_susanne);
        glUniform1f(glGetUniformLocation(program, "rotationRayFix"), 13);
        glUniform1f(glGetUniformLocation(program, "colorType"), 2); //YELLOW
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL
        glUniform1f(glGetUniformLocation(program, "rotator"), 52 + rotator_susanne);
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL
        glUniform1f(glGetUniformLocation(program, "rotator"), 92 + rotator_susanne);
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL
        glUniform1f(glGetUniformLocation(program, "rotator"), 132 + rotator_susanne);
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL
        glUniform1f(glGetUniformLocation(program, "rotator"), 172 + rotator_susanne);
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL
        glUniform1f(glGetUniformLocation(program, "rotator"), 212 + rotator_susanne);
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL
        glUniform1f(glGetUniformLocation(program, "rotator"), 252 + rotator_susanne);
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL
        glUniform1f(glGetUniformLocation(program, "rotator"), 292 + rotator_susanne);
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL
        glUniform1f(glGetUniformLocation(program, "rotator"), 332 + rotator_susanne);
        glDrawArrays(GL_TRIANGLES, 0, vertSize_susanne);  // DRAW CALL


        glBindVertexArray(vao_plane);
            glUniform3f(glGetUniformLocation(program, "localToWorldTransf"), 0, -1.65, -5);
            glUniform1f(glGetUniformLocation(program, "rotator"), 0);
            glUniform1f(glGetUniformLocation(program, "rotationRayFix"), 0);
            glUniform1f(glGetUniformLocation(program, "fatness"), 0);
            glUniform1f(glGetUniformLocation(program, "scale"), 2);
            glUniform1i(glGetUniformLocation(program, "useTexture"), GL_FALSE);
            glUniform1f(glGetUniformLocation(program, "colorType"), 1); //GRAY
        glDrawArrays(GL_TRIANGLES, 0, vertSize_plane);  // DRAW CALL
        
        aiv_opengl_context_swap(&context); //SWAP porta il framebuffer a schermo
    }

    aiv_opengl_context_destroy(&context);
    return 0;
}